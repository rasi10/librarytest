/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.helpers;

import se.nackademin.librarytest.pages.browsepages.BrowseBooksPage;
import static com.codeborne.selenide.Selenide.page;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import se.nackademin.librarytest.pages.*;
import se.nackademin.librarytest.pages.browsepages.BrowseAuthorsPage;
import se.nackademin.librarytest.pages.MenuPage;
import se.nackademin.librarytest.pages.SignInPage;
import se.nackademin.librarytest.pages.edit.EditBooksPage;
import se.nackademin.librarytest.pages.searchresults.AuthorResultSearchPage;
import se.nackademin.librarytest.pages.searchresults.BooksResultSearchPage;



/**
 *
 * @author rafael
 */
public class AdminHelper {
    public static void loginAsAdmin(){
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToSignIn();
        SignInPage signInPage = page(SignInPage.class);
        signInPage.setUsername("admin");
        signInPage.setPassword("1234567890");
        signInPage.clickLogIn();
    }
    
    public static void singOut(){
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToSignOut();
    }
    
    public static void createNewBook(String title, String datePublished){
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToAddABook();        
        AddABookPage addBookPage = page(AddABookPage.class);
        addBookPage.setTitle(title);
        addBookPage.setDatePublished(datePublished);        
        addBookPage.clickAddBookButton();  
        
        
    }
    
    public static void createNewAuthor(String firstName, String lastName, String country, String biography) {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToAddAnAuthor();        
        
        AddAuthorPage addAuthorPage = page(AddAuthorPage.class);
        addAuthorPage.setFirstName(firstName);
        addAuthorPage.setLastName(lastName);        
        addAuthorPage.setCountry(country);
        addAuthorPage.setBiography(biography);
        addAuthorPage.clickAddAuthorButton();
       
    }
    
    public static String fetchRecentlyAddedBook(String title) {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseBooks();
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);
        
        browseBooksPage.setTitleField(title);
        browseBooksPage.clickSearchBooksButton();
        browseBooksPage.clickFirstResultTitle();       
        
        String titleOfTheBook = browseBooksPage.getTitleField();
        
        return titleOfTheBook;
                
    }
    
     public static String editRecentlyCreatedBook(String title, String newDatePublished) {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseBooks();
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);
        
        browseBooksPage.setTitleField(title);
        browseBooksPage.clickSearchBooksButton();
        browseBooksPage.clickFirstResultTitle();  
        BooksResultSearchPage booksResultsSearchPage = page (BooksResultSearchPage.class);
        booksResultsSearchPage.clickEditBookButton();
        
        AddABookPage addBookPage = page(AddABookPage.class);
        
        addBookPage.setDatePublished(newDatePublished);
        addBookPage.clickSaveBookButton();
        
        
        menuPage.navigateToBrowseBooks();
        browseBooksPage.setTitleField(title);
        browseBooksPage.clickSearchBooksButton();
        browseBooksPage.clickFirstResultTitle();
        
        String datePublished = booksResultsSearchPage.getPublishDateOfTheBook();
        
        return datePublished;
                
    }
    
    
    public static void fetchAuthor(String name, String country) {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseAuthors();
        
        BrowseAuthorsPage browseAuthorsPage = page(BrowseAuthorsPage.class);     
        
        browseAuthorsPage.setNameField(name);
        browseAuthorsPage.setCountryField(country);
        browseAuthorsPage.clickSearchAuthorsButton();
        browseAuthorsPage.clickFirstResultTitle();        
    }
    
    
     public static String fetchAuthorsBiography(String name, String country) {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseAuthors();
        
        BrowseAuthorsPage browseAuthorsPage = page(BrowseAuthorsPage.class);     
        
        browseAuthorsPage.setNameField(name);
        browseAuthorsPage.setCountryField(country);
        browseAuthorsPage.clickSearchAuthorsButton();
        browseAuthorsPage.clickFirstResultTitle();  
        AuthorResultSearchPage authorResult = page (AuthorResultSearchPage.class);
        String biographyText = authorResult.getAuthorBiography();        
        return biographyText;
    }
     
     
    public static void EditAuthor() {
        AuthorResultSearchPage authoResulPage = page(AuthorResultSearchPage.class);
        authoResulPage.clickEditAuthorButton();
        
        AddAuthorPage addAuthorPage = page(AddAuthorPage.class);
        addAuthorPage.setBiography("New edited biography");
        addAuthorPage.clickSaveAuthorButton();
                
    }
    
     public static boolean editPublishDateOfABook(String book){
        GenerateRandomDate generateDate = new GenerateRandomDate();
        
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseBooks();
        
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);
        browseBooksPage.setTitleField(book);
        browseBooksPage.clickSearchBooksButton();
        browseBooksPage.clickFirstResultTitle();
                
        BooksResultSearchPage booksResultSearchPage = page(BooksResultSearchPage.class);
        String publishDateBeforeEdit = booksResultSearchPage.getPublishDateOfTheBook2();
        
        booksResultSearchPage.clickEditBookButton();
                 
        //work on the editbookspage
        EditBooksPage editBooksPage = page(EditBooksPage.class);
        editBooksPage.setPublishDate(generateDate.generateRandomPublishDateDate());
        editBooksPage.clickSaveChangesButton();
        
        
        menuPage.navigateToBrowseBooks();        
        browseBooksPage = page(BrowseBooksPage.class);
        browseBooksPage.setTitleField(book);
        browseBooksPage.clickSearchBooksButton();
        browseBooksPage.clickFirstResultTitle();
        String publishDateAfterEdit = booksResultSearchPage.getPublishDateOfTheBook2();
        System.out.println("PUBLISH DATEEEEEEEE: "+publishDateBeforeEdit);
        System.out.println("PUBLISH DATEEEEEEEE: "+publishDateAfterEdit);
        
        
        
        if (!publishDateBeforeEdit.equals(publishDateAfterEdit)){
            return true;
        }
        
        return false;
            
     }       
     
     public static void createNewLibrarianUser(String displayName, String password){
        MenuPage menuPage = page(MenuPage.class);       
        menuPage.navigateToAddUser();

        AddUserPage addUserPage = page(AddUserPage.class);
        addUserPage.setUsername(displayName);
        addUserPage.setPassword(password);
        addUserPage.clickLibrarianRadioButton();        
        addUserPage.clickAddUserButton();
        
        
    }
     
      public static void createNewLoanerUser(String displayName, String password){
        MenuPage menuPage = page(MenuPage.class);       
        menuPage.navigateToAddUser();

        AddUserPage addUserPage = page(AddUserPage.class);
        addUserPage.setUsername(displayName);
        addUserPage.setPassword(password);
        addUserPage.clickLoanerRadioButton();        
        addUserPage.clickAddUserButton();
        
        
    }
    
}
