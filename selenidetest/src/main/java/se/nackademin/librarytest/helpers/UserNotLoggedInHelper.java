/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.helpers;

import static com.codeborne.selenide.Selenide.page;
import se.nackademin.librarytest.pages.MenuPage;
import se.nackademin.librarytest.pages.MyProfilePage;
import se.nackademin.librarytest.pages.browsepages.BrowseAuthorsPage;
import se.nackademin.librarytest.pages.browsepages.BrowseBooksPage;
import se.nackademin.librarytest.pages.searchresults.BooksResultSearchPage;

/**
 *
 * @author rafael
 */
public class UserNotLoggedInHelper {
    
    public static void browseABookNotLoggedIn() {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseAuthors();         
        BrowseAuthorsPage browseAuthors = page(BrowseAuthorsPage.class);
        browseAuthors.clickSearchAuthorsButton();
        browseAuthors.clickFirstResultTitle();                
        BrowseBooksPage browseBooksPage = page(BrowseBooksPage.class);         
        browseBooksPage.clickFirstResultTitle();       
        BooksResultSearchPage booksResultSearchPage = page(BooksResultSearchPage.class);      
        
       
    } 
    
    public static void browseAuthorsNotLoggedIn() {
        MenuPage menuPage = page(MenuPage.class);
        menuPage.navigateToBrowseAuthors();         
        BrowseAuthorsPage browseAuthors = page(BrowseAuthorsPage.class);
        browseAuthors.clickSearchAuthorsButton();      
       
    } 
    
}
