/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author testautomatisering
 */
public class AddUserPage extends MenuPage {
    @FindBy(css = "#gwt-uid-3")
    SelenideElement userNameField;
    @FindBy(css = "#gwt-uid-5")
    SelenideElement passwordField;
    @FindBy(css = "#add-user-button")
    SelenideElement addUserButton;
    
    @FindBy(css = "#gwt-uid-16")
    SelenideElement librarianRadioButton;
    
    @FindBy(css = "#gwt-uid-17")
    SelenideElement loanerRadioButton;

    public void setUsername(String username) {
        setTextFieldValue("username", username, userNameField);
    }

    public void setPassword(String password) {
        setTextFieldValue("password", password, passwordField);
    }

    public void clickAddUserButton() {
        clickButton("add user button", addUserButton);
    }
    
    public void clickLibrarianRadioButton() {
        clickButton("Librarian radio button", librarianRadioButton);
    }
    
    public void clickLoanerRadioButton() {
        clickButton("Loaner radio button", loanerRadioButton);
    }
}
