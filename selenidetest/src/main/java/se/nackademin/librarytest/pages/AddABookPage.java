/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author rafael
 */
public class AddABookPage extends MenuPage {
    @FindBy(css = "#gwt-uid-3")
    SelenideElement titleField;
    
    @FindBy(css = "#gwt-uid-5")
    SelenideElement numberInInventoryField;
     
    @FindBy(css = "#gwt-uid-7")
    SelenideElement datePublishedField;
      
    @FindBy(css = "#gwt-uid-9")
    SelenideElement descriptionField;
    
    @FindBy(css = "#gwt-uid-11")
    SelenideElement numberOfPagesField;
    
    @FindBy(css = "#gwt-uid-13")
    SelenideElement isbnField;
    
    @FindBy(css="#add-book-button")
    SelenideElement addBookButton;
    
    @FindBy(css="#save-book-button")
    SelenideElement saveChangesButton;
    
    public void setTitle(String title) {
       setTextFieldValue("title", title, titleField);
      
    }
    
    public void setNumberInInventory(String numberInInventory) {
       setTextFieldValue("number in inventory", numberInInventory, numberInInventoryField);
      
    }
    
    public void setDatePublished(String datePublished) {
       setTextFieldValue("date published", datePublished, datePublishedField);
      
    }
    
    public void setDescription(String description) {
       setTextFieldValue("description", description, descriptionField);
      
    }
    
    public void setNumberOfPages(String numberOfPages) {
       setTextFieldValue("Number of pages", numberOfPages, numberOfPagesField);
      
    }
    
    public void setIsbn(String isbn) {
       setTextFieldValue("isbn", isbn, isbnField);
      
    }
    
    public void clickAddBookButton() {
        clickButton("add book button", addBookButton);
    }
    public void clickSaveBookButton() {
        clickButton("save book button", saveChangesButton);
    }
}
