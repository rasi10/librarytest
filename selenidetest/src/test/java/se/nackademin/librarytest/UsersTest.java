/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest;

import static com.codeborne.selenide.Selenide.sleep;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;
import se.nackademin.librarytest.helpers.UserHelper;

/**
 *
 * @author rafael
 */
public class UsersTest extends TestBase{
    
    public UsersTest() {
    }
    
    @Test
    public void testEditUsersEmailAdress(){        
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);   
        UserHelper.createNewUser(values, values);
        UserHelper.logInAsUser(values, values);
        boolean resultOfEditOperation = UserHelper.editUserProfileEmail(values, values, "new email");
        assertEquals("The edit email operation should be successfull!",resultOfEditOperation, true);
        UserHelper.singOut();
        sleep(500);
    } 
    
    @Test
    public void testEditUsersFirstName(){        
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);   
        UserHelper.createNewUser(values, values);
        UserHelper.logInAsUser(values, values);
        boolean resultOfEditOperation = UserHelper.editUserProfileFirstName(values, values, "new first name");
        assertEquals("The edit first name operation should be successfull!",resultOfEditOperation, true);
        UserHelper.singOut();
        sleep(500);
    } 
    
    @Test
    public void testEditUsersLastName(){        
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);   
        UserHelper.createNewUser(values, values);
        UserHelper.logInAsUser(values, values);
        boolean resultOfEditOperation = UserHelper.editUserProfileLastName(values, values, "new last name");
        assertEquals("The edit last name operation should be successfull!",resultOfEditOperation, true);
        UserHelper.singOut();
        sleep(500);
    } 
    
    @Test
    public void testEditPhoneNumber(){        
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);   
        UserHelper.createNewUser(values, values);
        UserHelper.logInAsUser(values, values);
        boolean resultOfEditOperation = UserHelper.editUserProfilePhone(values, values, "new phone number");
        assertEquals("The edit phone number operation should be successfull!",resultOfEditOperation, true);
        UserHelper.singOut();
        sleep(500);
    } 
       
    @Test
    public void testBorrowABook(){        
       String values = UUID.randomUUID().toString();
       values = values.substring(0,6);   
       UserHelper.createNewUser(values, values);
       UserHelper.logInAsUser(values, values);
       boolean resultOfBorrowingABook = UserHelper.borrowABook();
       assertTrue("The result of the operation should be true!",resultOfBorrowingABook);
       UserHelper.singOut();
       sleep(500);
    }
    
    @Test
    public void testBorrowABook2(){        
       String values = UUID.randomUUID().toString();
       values = values.substring(0,6);   
       UserHelper.createNewUser(values, values);
       UserHelper.logInAsUser(values, values);
       boolean resultOfBorrowingABook = UserHelper.borrowABook2();
       assertTrue("The result of the operation should be true!",resultOfBorrowingABook);
       UserHelper.singOut();
       sleep(500);
    }
    
    @Test
    public void testBorrowABook3(){        
       String values = UUID.randomUUID().toString();
       values = values.substring(0,6);   
       UserHelper.createNewUser(values, values);
       UserHelper.logInAsUser(values, values);
       boolean resultOfBorrowingABook = UserHelper.borrowABook3();
       assertTrue("The result of the operation should be true!",resultOfBorrowingABook);
       UserHelper.singOut();
       sleep(500);
    }
    
}
