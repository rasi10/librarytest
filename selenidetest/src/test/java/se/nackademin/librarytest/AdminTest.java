/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.source;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;
import se.nackademin.librarytest.helpers.AdminHelper;
import se.nackademin.librarytest.helpers.GenerateRandomDate;
import se.nackademin.librarytest.helpers.UserHelper;
import se.nackademin.librarytest.pages.searchresults.AuthorResultSearchPage;

/**
 *
 * @author rafael
 */
public class AdminTest extends TestBase{
    
    public AdminTest() {
    }
      
    
    @Test
    public void testCreateABook(){        
        String title = UUID.randomUUID().toString();
        title = title.substring(0,6);
        GenerateRandomDate generateRandomDate = new GenerateRandomDate();
        String datePublished = generateRandomDate.generateRandomPublishDateDate();
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewBook(title, datePublished);
        String titleOfTheBook = AdminHelper.fetchRecentlyAddedBook(title);
        assertEquals("The title of the book should be the same", title, titleOfTheBook);     
        AdminHelper.singOut();
        sleep(500); 
    }  
    
    @Test
    public void testEditABook(){        
        String title = UUID.randomUUID().toString();
        title = title.substring(0,6);
        GenerateRandomDate generateRandomDate = new GenerateRandomDate();
        String datePublished = generateRandomDate.generateRandomPublishDateDate();
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewBook(title, datePublished);
        String titleOfTheBook = AdminHelper.fetchRecentlyAddedBook(title);        
        assertEquals("The title of the book should be the same", title, titleOfTheBook); 
       
        String newDatePublished = generateRandomDate.generateRandomPublishDateDate();
        String datePublishedRetrieved = AdminHelper.editRecentlyCreatedBook(title, newDatePublished);
        assertEquals("The publish date should have changed", datePublishedRetrieved, newDatePublished);
        AdminHelper.singOut();
        sleep(500); 
    }  
    
    @Test
    public void testCreateANewAuthor(){        
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewAuthor(values,values, values, values);
        AdminHelper.fetchAuthor(values, values);        
        AuthorResultSearchPage authorResults = page(AuthorResultSearchPage.class);         
        assertEquals("The name of the author should be displayed!",authorResults.getAuthorName().contains(values),true);
        AdminHelper.singOut();
        sleep(500);        
    } 
    
    @Test 
    public void testEditAnAuthor(){
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewAuthor(values,values, values, values);
        AdminHelper.fetchAuthor(values, values);        
        AuthorResultSearchPage authorResults = page(AuthorResultSearchPage.class);         
        assertEquals("The name of the author should be displayed!",authorResults.getAuthorName().contains(values),true);
        
        AdminHelper.fetchAuthor(values, values);
        AdminHelper.EditAuthor();
        String newBio = AdminHelper.fetchAuthorsBiography(values, values);
        assertEquals("The biography of the author should be new biography!", newBio,"New edited biography");
        AdminHelper.singOut();
        sleep(500); 
    }
    
    @Test
    public void testCreateALibrarianUser(){
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewLibrarianUser(values, values);        
        AdminHelper.singOut();
        
        UserHelper.logInAsUser(values, values);
        String usersDisplayName = UserHelper.fetchUsersDisplayName();
        assertEquals("The user name should be the one created by the admin", usersDisplayName, values);
        AdminHelper.singOut();
        sleep(500); 
    }
    
    @Test
    public void testCreateALoanerUser(){
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);
        AdminHelper.loginAsAdmin();
        AdminHelper.createNewLoanerUser(values, values);        
        AdminHelper.singOut();
        
        UserHelper.logInAsUser(values, values);
        String usersDisplayName = UserHelper.fetchUsersDisplayName();
        assertEquals("The user name should be the one created by the admin", usersDisplayName, values);
        AdminHelper.singOut();
        sleep(500); 
    }
    
    
}
