package se.nackademin.librarytest;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.*;
import java.util.UUID;

import org.junit.Test;
import static org.junit.Assert.*;
import se.nackademin.librarytest.helpers.AdminHelper;
import se.nackademin.librarytest.helpers.UserHelper;
import se.nackademin.librarytest.pages.MenuPage;

/**
 *
 * @author rafael
 */
public class NavigationTest extends TestBase {
    
    public NavigationTest() {
    }
    
    @Test
    public void testNavigateMenuPage(){
        MenuPage menuPage = page(MenuPage.class);
        assertEquals("The URL should contain librarytest", url().contains("librarytest"), true);
        menuPage.navigateToBrowseBooks();          
        assertEquals("The URL should contain books", url().contains("books"), true);
        menuPage.navigateToBrowseAuthors();
        assertEquals("The URL should contain authors", url().contains("authors"), true);
        menuPage.navigateToAddUser();
        assertEquals("The URL should contain user", url().contains("user"), true);
        menuPage.navigateToSignIn();
        assertEquals("The URL should contain login", url().contains("login"), true);
        menuPage.navigateToSignOut();
        assertEquals("The URL should contain logout", url().contains("logout"), true);
        
    }
    
    @Test
    public void testNavigationMenuPageAsAdmin(){
        AdminHelper.loginAsAdmin();
        MenuPage menuPage = page(MenuPage.class);
        assertEquals("The URL should contain librarytest", url().contains("librarytest"), true);
        menuPage.navigateToBrowseBooks();          
        assertEquals("The URL should contain books", url().contains("books"), true);
        menuPage.navigateToBrowseAuthors();
        assertEquals("The URL should contain authors", url().contains("authors"), true);        
        menuPage.navigateToAddABook();
        assertEquals("The URL should contain add and book", url().contains("add")&& url().contains("book"), true);
        menuPage.navigateToAddAnAuthor();
        assertEquals("The URL should contain add and author", url().contains("add")&& url().contains("author"), true);
        menuPage.navigateToAddUser();
        assertEquals("The URL should contain user", url().contains("user"), true);
        menuPage.navigateToSignIn();   
         assertEquals("The URL should contain login", url().contains("login"), true);        
        menuPage.navigateToMyProfile();
        assertEquals("The URL should contain profile", url().contains("profile"), true);        
        menuPage.navigateToSignOut();
        assertEquals("The URL should contain logout", url().contains("logout"), true);        
                
    }
    
     @Test
    public void testNavigationMenuPageAsNormalUser(){
        MenuPage menuPage = page(MenuPage.class);
        String values = UUID.randomUUID().toString();
        values = values.substring(0,6);   
        UserHelper.createNewUser(values, values);
        UserHelper.logInAsUser(values, values);        
        
        assertEquals("The URL should contain librarytest", url().contains("librarytest"), true);
        menuPage.navigateToBrowseBooks();          
        assertEquals("The URL should contain books", url().contains("books"), true);
        menuPage.navigateToBrowseAuthors();
        assertEquals("The URL should contain authors", url().contains("authors"), true);        
        menuPage.navigateToSignIn();   
        assertEquals("The URL should contain login", url().contains("login"), true);
        menuPage.navigateToMyProfile();
        assertEquals("The URL should contain profile", url().contains("profile"), true);
        menuPage.navigateToSignOut();
        assertEquals("The URL should contain logout", url().contains("logout"), true);
    }
}
