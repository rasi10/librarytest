/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import org.junit.Test;
import static org.junit.Assert.*;
import se.nackademin.librarytest.helpers.UserNotLoggedInHelper;

/**
 *
 * @author rafael
 */
public class UserNotLoggedInTest extends TestBase{
    
    public UserNotLoggedInTest() {
    }
    
    @Test
    public void testBrowseABookNotLoggedIn(){        
       UserNotLoggedInHelper.browseABookNotLoggedIn();
       assertEquals("The URL should contain book", url().contains("book"), true);               
       sleep(500);
    
    }
    
    @Test
    public void testSearchForAuthorsNotLoggedIn(){        
       UserNotLoggedInHelper.browseAuthorsNotLoggedIn();
       assertEquals("The URL should contain authors", url().contains("authors"), true);               
       sleep(500);
    
    }
    
}
