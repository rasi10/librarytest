
package se.nackademin.librarytest.rafael;

import com.jayway.restassured.response.Response;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.logging.Logger;
/**
 *
 * @author rafael
 */
public class TestSuiteEndPointBooks {
    
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
        
    public TestSuiteEndPointBooks() {
    }
        
    /**
     TESTS FOR /BOOKS
     */
    
    
    @Test   
    public void getAllBooks(){   
        LOG.info("TEST 01 - GET ALL BOOKS");
        BookOperations bookOperations = new BookOperations();
        Response response = bookOperations.getAllBooks();
        assertEquals("Status code should be 200",200, bookOperations.getStatusCodeFromResponse(response));
    }
    
    @Test
    public void createANewBookWithAuthor(){
        LOG.info("TEST 02 - CREATE A NEW BOOK WITH AUTHOR");
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();      
        Response postResponse = authorOperations.createRandomAuthorWithNoId();       
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
    }
    
 
    @Test
    public void UpdateANewBookWithAuthor(){
        LOG.info("TEST 03 - UPDATING A BOOK");
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());  
        int idOfTheLastCreatedBook = bookOperations.getIdOfTheLastAddedBook();
        Response responseUpdateBook = bookOperations.updateBookWithAuthorId(idOfTheLastCreatedBook);
        assertEquals("POST response should have status code 200",200,responseUpdateBook.statusCode());  
    }
    
    
    @Test
    public void createANewBookWithAuthorInvalidAuthor(){
        LOG.info("TEST 04 - CREATE A NEW BOOK WITH AUTHOR - INVALID AUTHOR");       
        BookOperations bookOperations = new BookOperations();     
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(-1111);
        assertEquals("POST response should have status code 400",400,responseCreateBook.statusCode());  
       
    }    
    
    
    
    @Test
    public void UpdateANewBookWithAuthorInvalidBook(){
        LOG.info("TEST 05 - UPDATING A BOOK - INVALID BOOK ID");
        BookOperations bookOperations = new BookOperations();  
        Response responseUpdateBook = bookOperations.updateBookWithAuthorId(-1111);
        assertEquals("POST response should have status code 404",404,responseUpdateBook.statusCode());  
    }
    
    @Test
    public void UpdateANewBookWithAuthorInvalidAuthor(){
        LOG.info("TEST 06 - UPDATING A BOOK - INVALID BOOK - WITH NO TITLE");
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());  
        int idOfTheLastCreatedBook = bookOperations.getIdOfTheLastAddedBook();
        Response responseUpdateBook = bookOperations.updateBookWithAuthorIdInvalidBook(idOfTheLastCreatedBook);
        assertEquals("POST response should have status code 400",400,responseUpdateBook.statusCode());  
    }
    
    
    /**
     TESTS FOR /BOOKS/ID
     */
    
    @Test
    public void getABookById(){
        LOG.info("TEST 07 - GETTING A BOOK BY ITS ID");
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());  
        int idOfTheLastCreatedBook = bookOperations.getIdOfTheLastAddedBook();
        Response responseGetAbookByID = bookOperations.getABookById(idOfTheLastCreatedBook);
        assertEquals("POST response should have status code 200",200,responseGetAbookByID.statusCode());          
    }  
    
    @Test
    public void DeleteABookById(){
        LOG.info("TEST 08 - DELETING A BOOK BY ITS ID");
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());   
        int idOfTheLastCreatedBook = bookOperations.getIdOfTheLastAddedBook();
        Response responseDeleteAbookByID = bookOperations.deleteBookById(idOfTheLastCreatedBook);
        assertEquals("POST response should have status code 204",204,responseDeleteAbookByID.statusCode());          
    }
    
    @Test
    public void getABookByIdInvalidID(){
        LOG.info("TEST 09 - GETTING A BOOK BY ITS ID - INVALID ID");       
        BookOperations bookOperations = new BookOperations();       
        Response responseGetAbookByID = bookOperations.getABookById(-11111);
        assertEquals("POST response should have status code 404",404,responseGetAbookByID.statusCode());          
    }  
    
    @Test
    public void DeleteABookByIdInvalidID(){
        LOG.info("TEST 10 - DELETING A BOOK BY ITS ID - INVALID ID");
        BookOperations bookOperations = new BookOperations();
        Response responseDeleteAbookByID = bookOperations.deleteBookById(-11111);
        assertEquals("POST response should have status code 404",404,responseDeleteAbookByID.statusCode());          
    }
    
    
    
    /**
     TESTS FOR /BOOKS/BYAUTHOR/AUTHORID
     */
    @Test
    public void getAllBooksByAuthorId(){
        LOG.info("TEST 11 - GETTING ALL BOOKS BY AN AUTHOR");
        AuthorOperations authorOperations = new AuthorOperations(); 
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        Response response = bookOperations.getBooksByAuthorId(idOfThelastAddedAuthor);
        assertEquals("Status code should be 200",200, response.getStatusCode());
    }
    
   
   // @Test
    public void getAllBooksByAuthorIdInvalidID(){
        LOG.info("TEST 12 - GETTING ALL BOOKS BY AN AUTHOR - INVALID ID");        
        BookOperations bookOperations = new BookOperations();
        Response response = bookOperations.getBooksByAuthorId(-111111);
        assertEquals("Status code should be 404",404, response.getStatusCode());
    }
   
    
    /**
     * TESTS FOR /BOOKS/BOOKID/AUTHORS
     */
    @Test
    public void getAllAuthorsByBookIdId(){
        LOG.info("TEST 13 - GETTING ALL AUTHORS BY BOOK ID");
        AuthorOperations authorOperations = new AuthorOperations(); 
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        int idOfThelastAddedBook = bookOperations.getIdOfTheLastAddedBook();
        
        Response responseGetAuthorsByBookId = bookOperations.getAuthorsByBookId(idOfThelastAddedBook);
        assertEquals("Status code should be 200",200, responseGetAuthorsByBookId.getStatusCode());
    }
    
    @Test
    public void addAnAuthorToABook(){
        LOG.info("TEST 14  - ADDING A NEW AUTHOR TO A BOOK");
        AuthorOperations authorOperations = new AuthorOperations(); 
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());  
        postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());
        idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response resposeAddABook = bookOperations.addAnAuthorToABook(idOfThelastAddedAuthor, idOfTheLastBookCreated); 
        assertEquals("POST response should have status code 200",200,resposeAddABook.statusCode());
        
       
    }
     
    
    @Test
    public void editListOfAuthors(){
        LOG.info("TEST 15 - EDITING THE LIST OF AUTHORS OF A BOOK");
        AuthorOperations authorOperations = new AuthorOperations(); 
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());  
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());
        int idOfThelastAddedAuthor1 = authorOperations.getIdOfTheLastAddedAuthor();
        postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());
        int idOfThelastAddedAuthor2 = authorOperations.getIdOfTheLastAddedAuthor();
        Response resposeAddABook = bookOperations.editListOfAuthorsToABook(idOfThelastAddedAuthor1, idOfThelastAddedAuthor2, idOfTheLastBookCreated); 
        assertEquals("POST response should have status code 200",200,resposeAddABook.statusCode());
       
    }
    
    
    @Test
    public void getAllAuthorsByBookIdInvalidID(){
        LOG.info("TEST 16 - GETTING ALL AUTHORS BY BOOK ID - INVALID ID");
        BookOperations bookOperations = new BookOperations();        
        Response responseGetAuthorsByBookId = bookOperations.getAuthorsByBookId(-1111111);
        assertEquals("Status code should be 404",404, responseGetAuthorsByBookId.getStatusCode());
    }
    
    
}