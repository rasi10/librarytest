package se.nackademin.librarytest.rafael;

import static com.jayway.restassured.path.json.JsonPath.from;
import com.jayway.restassured.response.Response;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class TestSuiteEndPointAuthors {

    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    public TestSuiteEndPointAuthors() {
    }

    @Test
    public void getAllAuthors() {
        LOG.info("TEST 01 - GET ALL BOOKS");
        AuthorOperations authorOperations = new AuthorOperations();
        Response response = authorOperations.getAllAuthors();
        assertEquals("Status code should be 200", 200, response.getStatusCode());
    }

    @Test
    public void createARandomAuthorWithNoId() {
        LOG.info("TEST 02 - CREATE AN AUTHOR WITH NO ID");        
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("Post response should have status code 201", 201, postResponse.statusCode());
        String lastAddedAuthor = authorOperations.getNameOfTheLastAddedAuthor();
        String expectedAuthor = from(authorOperations.getJsonString()).getString("author.name");
        assertEquals("The name of the author should be the same!", expectedAuthor, lastAddedAuthor);
    }

    @Test
    public void createARandomAuthorWithId() {  
        LOG.info("TEST 03 - CREATE A RANDOM AUTHOR WITH NO ID");
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        String nameOfThelastAddedAuthor = authorOperations.getNameOfTheLastAddedAuthor();
        String expectedAuthor = from(authorOperations.getJsonString()).getString("author.name");
        assertEquals("The name of the author should be the same!", expectedAuthor, nameOfThelastAddedAuthor);

    }

    @Test
    public void creatingAnInvalidAuthorIdExists() {  
        LOG.info("TEST 04 - CREATE AN AUTHOR WHEN HE ALREADY EXISTS");
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        String nameOfThelastAddedAuthor = authorOperations.getNameOfTheLastAddedAuthor();
        String expectedAuthor = from(authorOperations.getJsonString()).getString("author.name");
        assertEquals("The name of the author should be the same!", expectedAuthor, nameOfThelastAddedAuthor);
        int idOfTheLastCreatedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        postResponse = authorOperations.createInvalidAuthorExistingIdOnTheDatabase(idOfTheLastCreatedAuthor);
        assertEquals("Should return 400 as the authors id already exists!", 400, postResponse.statusCode());
    }

    @Test
    public void gettingAnAuthorById() {
        LOG.info("TEST 05 - GETTING AN AUTHOR BY HIS ID");        
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        int idOfTheLastCreatedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response getResponse = authorOperations.getAuthorById(idOfTheLastCreatedAuthor);
        assertEquals("The status code should be 200!", 200, getResponse.getStatusCode());
    }

    @Test
    public void gettingAnAuthorByIdInvalidID() {
        LOG.info("TEST 06 - GETTING AN AUTHOR BY ID - INVALID ID");
        AuthorOperations authorOperations = new AuthorOperations();
        Response getResponse = authorOperations.getAuthorById(-11111);
        assertEquals("The status code should be 404!", 404, getResponse.getStatusCode());

    }

    @Test
    public void deleteAuthorExistingId() { 
        LOG.info("TEST 08 - DELETING AN AUTHOR BY HIS ID");
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        int idOfTheLastCreatedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response getResponse = authorOperations.deleteAuthor(idOfTheLastCreatedAuthor);
        assertEquals("The status code should be 204!", 204, getResponse.getStatusCode());
    }

    @Test
    public void deleteAuthorNotExistingId() {
        LOG.info("TEST 09 - DELETING AN AUTHOR BY HIS ID - INVALID ID");
        AuthorOperations authorOperations = new AuthorOperations();
        Response getResponse = authorOperations.deleteAuthor(-11111);
        assertEquals("The status code should be 404!", 404, getResponse.getStatusCode());
    }

    @Test
    public void editTheNameOfAnAuthor() {  
        LOG.info("TEST 10 - EDITING THE NAME OF AN AUTHOR");
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        String nameOfThelastAddedAuthor = authorOperations.getNameOfTheLastAddedAuthor();
        String expectedAuthor = from(authorOperations.getJsonString()).getString("author.name");
        assertEquals("The name of the author should be the same!", expectedAuthor, nameOfThelastAddedAuthor);
        int idOfTheLastCreatedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response putResponse = authorOperations.editAuthorWithMyName(idOfTheLastCreatedAuthor);
        assertEquals("Post response should have status code 200", 200, putResponse.statusCode());
        nameOfThelastAddedAuthor = authorOperations.getFirstNameOfTheLastAddedAuthor();
        expectedAuthor = "Rafael";
        assertEquals("The name of the author should be the same!", expectedAuthor, nameOfThelastAddedAuthor);
    }

    @Test
    public void editTheNameOfAnAuthorInvalidAuthorId() {  
        LOG.info("EDITING THE NAME OF THE AUTHOR - INVALID AUTHOR ID");
        AuthorOperations authorOperations = new AuthorOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201", 201, postResponse.statusCode());
        String nameOfThelastAddedAuthor = authorOperations.getNameOfTheLastAddedAuthor();
        String expectedAuthor = from(authorOperations.getJsonString()).getString("author.name");
        assertEquals("The name of the author should be the same!", expectedAuthor, nameOfThelastAddedAuthor);        
        Response putResponse = authorOperations.editAuthorWithMyName(-111111);
        assertEquals("PUT response should have status code 400", 404, putResponse.statusCode());

    }
}
