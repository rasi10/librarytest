/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.rafael;

import com.jayway.restassured.response.Response;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class TestSuiteEnpointLoans {
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
        
    public TestSuiteEnpointLoans() {
    }
    
    /**
     TESTS FOR /LOANS
     */
    @Test   
    public void getAllLoans(){   
        LOG.info("TEST 01 - GET ALL LOANS");
        LoansOperations loansOperations = new LoansOperations();
        Response response = loansOperations.getAllLoans();        
        assertEquals("Status code should be 200",200, response.getStatusCode());
    }
    
    @Test
    public void createANewLoan(){
        LOG.info("TEST 02 - CREATE A NEW LOAN");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
    }
    
    @Test
    public void createANewLoanInvalidUser(){
        LOG.info("TEST 03 - CREATE A NEW LOAN - INVALID USER");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode()); 
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, -1111111);
        assertEquals("The status code should be 400",400,postNewLoan.getStatusCode());
    }
    
    @Test
    public void createANewLoanLoanBorrowingAllTheBooks(){
        LOG.info("TEST 04 - CREATE A NEW LOAN - BORROWING ALL THE COPIES OF A BOOK");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 409",409,postNewLoan.getStatusCode());
       
    }
    
    /**
     TESTS FOR /LOANS/ID
     */
    
    //@Test
    public void getALoanByID(){
        LOG.info("TEST 05 - GET A LOAN BY ITS ID");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        int idOfTheLastLoan = loansOperations.getIdOfTheLastAddedLoan();
        Response getResponse = loansOperations.getALoanById(idOfTheLastLoan);        
        assertEquals("The status code should be 200",200,getResponse.getStatusCode());       
    }
    
    //@Test
    public void deleteALoanByID(){
        LOG.info("TEST 06 - DELETE A LOAN BY ITS ID");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        int idOfTheLastLoan = loansOperations.getIdOfTheLastAddedLoan();
        Response deleteResponse = loansOperations.deleteLoanById(idOfTheLastLoan); 
        assertEquals("The status code should be 204",204,deleteResponse.getStatusCode());       
    }
    
    @Test
    public void getALoanByIDInvalidId(){
        LOG.info("TEST 07 - GET A LOAN BY ITS ID - INVALID ID");
        LoansOperations loansOperations = new LoansOperations();        
        Response getResponse = loansOperations.getALoanById(-11111);        
        assertEquals("The status code should be 404",404,getResponse.getStatusCode());       
    }
    
    @Test
    public void deleteALoanByIDinvalidId(){
        LOG.info("TEST 08 - DELETE A LOAN BY ITS ID - INVALID ID");
        LoansOperations loansOperations = new LoansOperations();       
        Response deleteResponse = loansOperations.deleteLoanById(-11111); 
        assertEquals("The status code should be 404",404,deleteResponse.getStatusCode());       
    }
    
    
    
    /**
     TESTS FOR /LOANS/OFUSER/USERID     
     */
    
    @Test
    public void getLoansOfUserByUserId(){
        LOG.info("TEST 09 - GET LOANS OF A USER BY THE USER'S ID");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        Response responseGetLoansOfUser = loansOperations.getLoansOfAUserByUserId(idOfTheLastUserCreated);
        assertEquals("The status code should be 200",200,responseGetLoansOfUser.statusCode());
        
    }
    
    @Test
    public void getLoansOfUserByUserIdInvalidUserId(){
        LOG.info("TEST 10 - GET LOANS OF A USER BY THE USER'S ID - INVALID USER ID");
        LoansOperations loansOperations = new LoansOperations();       
        Response responseGetLoansOfUser = loansOperations.getLoansOfAUserByUserId(-11111);
        assertEquals("The status code should be 404",404,responseGetLoansOfUser.statusCode());
        
    }
    
    /**
     TESTS FOR /LOANS/OFBOOK/BOOKID     
     */
    @Test
    public void getLoansOfBookBookId(){
        LOG.info("TEST 11 - GET LOANS OF A BOOK BY BOOKS ID");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        Response responseGetLoansOfBook = loansOperations.getLoansOfBookByBookId(idOfTheLastBookCreated);
        assertEquals("The status code should be 200",200,responseGetLoansOfBook.statusCode());
        
    }
    
    @Test
    public void getLoansOfBookBookIdInvalidId(){
        LOG.info("TEST 12 - GET LOANS OF A BOOK BY BOOKS ID - INVALID BOOK ID");
        LoansOperations loansOperations = new LoansOperations();       
        Response responseGetLoansOfBook = loansOperations.getLoansOfBookByBookId(-1111111);
        assertEquals("The status code should be 404",404,responseGetLoansOfBook.statusCode());
        
    }
        
    
    /**
     TESTS FOR /LOANS/OFUSER/USERID/OFBOOK/BOOKID
     
     */
    @Test
    public void getLoansOfUserAndBookBookByIds(){
        LOG.info("TEST 13 - GET THE LOANS OF A USER AND A BOOK BY THEIR IDS");
        LoansOperations loansOperations = new LoansOperations();
        AuthorOperations authorOperations = new AuthorOperations();   
        BookOperations bookOperations = new BookOperations();
        Response postResponse = authorOperations.createRandomAuthorWithNoId();
        assertEquals("POST response should have status code 201",201,postResponse.statusCode());       
        int idOfThelastAddedAuthor = authorOperations.getIdOfTheLastAddedAuthor();
        Response responseCreateBook = bookOperations.createRandomBookWithAuthorId(idOfThelastAddedAuthor);
        assertEquals("POST response should have status code 201",201,responseCreateBook.statusCode());         
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int idOfTheLastUserCreated = usersOperations.getIdOfTheLastAddedUser();
        int idOfTheLastBookCreated = bookOperations.getIdOfTheLastAddedBook();
        Response postNewLoan = loansOperations.createANewLoan(idOfTheLastBookCreated, idOfTheLastUserCreated);
        assertEquals("The status code should be 201",201,postNewLoan.getStatusCode());
        Response responseGetLoansOfBook = loansOperations.getLoansOfUserAndBookByUsersAndBooksId(idOfTheLastUserCreated,idOfTheLastBookCreated);
        assertEquals("The status code should be 200",200,responseGetLoansOfBook.statusCode());
        
    }
    
    @Test
    public void getLoansOfUserAndBookBookByIdsInvalidIds(){
        LOG.info("TEST 14 - GET THE LOANS OF A USER AND A BOOK BY THEIR IDS");
        LoansOperations loansOperations = new LoansOperations();
       
        Response responseGetLoansOfBook = loansOperations.getLoansOfUserAndBookByUsersAndBooksId(-11111,-111111);
        assertEquals("The status code should be 404",404,responseGetLoansOfBook.statusCode());
        
    }
}
