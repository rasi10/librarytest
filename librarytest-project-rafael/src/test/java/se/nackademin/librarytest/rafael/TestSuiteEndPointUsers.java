/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.rafael;

import com.jayway.restassured.response.Response;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.logging.Logger;
/**
 *
 * @author rafael
 */
public class TestSuiteEndPointUsers {
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    
    public TestSuiteEndPointUsers() {
    }
    
    /**
     TESTS FOR /USERS
     */
    
    @Test   
    public void getAllUsers(){  
        LOG.info("TEST 01 - GETTING ALL BOOKS");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.getAllUsers();
        assertEquals("Status code should be 200",200, response.getStatusCode());
    }
    
    @Test
    public void createANewUser(){
        LOG.info("TEST 02 - CREATING A NEW USER");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());        
    }     
    
    
    @Test
    public void editUser(){
        LOG.info("TEST 03 - EDITING A USER RECENTLY CREATED");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());        
        int ifOfTheLastUserAdded = usersOperations.getIdOfTheLastAddedUser();
        String displayNameLastUserAdded = usersOperations.getNameOfTheLastAddedUser();
        String passwordLastUserAdded = usersOperations.getEmailOfTheLastAddedUser();
        Response responseOfEdit = usersOperations.editUserNewUser(ifOfTheLastUserAdded,displayNameLastUserAdded, passwordLastUserAdded);
        assertEquals("Status code should be 200",200, responseOfEdit.getStatusCode()); 
        assertEquals("The display name should be Rafael",usersOperations.getNameOfTheLastAddedUser().contains("Rafael"), true);
    } 
    
    
    
    @Test
    public void createANewUserWithNoPassword(){
        LOG.info("TEST 04 - CREATING A NEW USER - INVALID USER - NO PASSWORD");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUserWithNoPassword();
        assertEquals("Status code should be 400",400, response.getStatusCode());        
    }
    
    @Test
    public void editUserInvalidUser(){
        LOG.info("TEST 05 - EDITING A USER RECENTLY CREATED - INVALID USER ID");
        UsersOperations usersOperations = new UsersOperations();       
        Response responseOfEdit = usersOperations.editUserNewUser(-1111111,"displayNameLastUserAdded", "passwordLastUserAdded");
        assertEquals("Status code should be 404",404, responseOfEdit.getStatusCode()); 
        
    } 
    
    @Test
    public void editUserNoDisplayNameNoPassword(){
        LOG.info("TEST 06 - EDITING A USER RECENTLY CREATED - INVALID - NO NAME NO PASSWORD");
        UsersOperations usersOperations = new UsersOperations(); 
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());        
        int ifOfTheLastUserAdded = usersOperations.getIdOfTheLastAddedUser();
        String displayNameLastUserAdded = usersOperations.getNameOfTheLastAddedUser();
        String passwordLastUserAdded = usersOperations.getEmailOfTheLastAddedUser();      
        Response responseOfEdit = usersOperations.editUserNewUserNoNameNoPassword(ifOfTheLastUserAdded);
        assertEquals("Status code should be 400",400, responseOfEdit.getStatusCode()); 
        
    } 
    
    
    /**     
     TESTS FOR /USERS/ID
     */
    
    @Test
    public void getUserById(){
        LOG.info("TEST 07 - GET A USER BY HIS ID");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());
        int ifOfTheLastUserAdded = usersOperations.getIdOfTheLastAddedUser();
        Response responseGetUserByID = usersOperations.getAUserById(ifOfTheLastUserAdded);
        assertEquals("Status code should be 200",200, responseGetUserByID.getStatusCode()); 
        
    } 
    
    @Test
    public void deleteUserById(){
        LOG.info("TEST 08 - DELETING A USER BY HIS ID");
        UsersOperations usersOperations = new UsersOperations();
        Response response = usersOperations.createANewUser();
        assertEquals("Status code should be 201",201, response.getStatusCode());        
        int ifOfTheLastUserAdded = usersOperations.getIdOfTheLastAddedUser();
        Response responseDeleteUserByID = usersOperations.deleteBookById(ifOfTheLastUserAdded);
        assertEquals("Status code should be 200",204, responseDeleteUserByID.getStatusCode());         
    } 
    
    @Test
    public void getUserByIdInvalidId(){
        LOG.info("TEST 09 - GET A USER BY HIS ID - INVALID ID");
        UsersOperations usersOperations = new UsersOperations();        
        Response responseGetUserByID = usersOperations.getAUserById(-111111);
        assertEquals("Status code should be 404",404, responseGetUserByID.getStatusCode());       
    } 
    
    @Test
    public void deleteUserByIdInvalidId(){
        LOG.info("TEST 08 - DELETING A USER BY HIS ID - INVALID ID");
        UsersOperations usersOperations = new UsersOperations();        
        Response responseDeleteUserByID = usersOperations.deleteBookById(-11111111);
        assertEquals("Status code should be 404",404, responseDeleteUserByID.getStatusCode());         
    } 
}

