/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.rafael;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class LoansOperations {
    private static final String BASE_URL = "http://localhost:8080/librarytest-rest/";    
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    private String jsonString = "";
    
    public void setJsonString(String string){
        this.jsonString = string;
    }
    public String getJsonString(){
        return this.jsonString;
    }
         
    public Response getAllLoans(){
        LOG.info("Making a GET request to retrieve all loans");
        String resourceName = "loans";
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
        
    public Response createANewLoan(int bookId, int userId){
        LOG.info("Making a POST request to create a new loan");
        String resourceName = "loans";
        String postBodyTemplate = "{\n" +
                "  \"loan\": {\n" +
                "    \"book\": {\n" +
                "      \"id\":\"%s\",\n" +
                "    },\n" +
                "    \"user\": {\n" +
                "      \"id\":\"%s\",\n" +
                "    }\n" +
                "  }\n" +
                "}";

        String postBody = String.format(postBodyTemplate, bookId, userId);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;
    } 
    
    
    public int getIdOfTheLastAddedLoan(){
        LOG.info("retrieving the id of the last added loan");
        Response getResponse = getAllLoans();           
        int fetchedId = getResponse.jsonPath().getInt("loans.loan[-1].id");         
        return fetchedId;
    }
    
    public Response getALoanById(int id){
        LOG.info("Making a GET request to get a loan by its ID");
        String resourceName = "loans/"+id;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response deleteLoanById(int id){
        LOG.info("Making a DELETE request to delete a loan by its ID");
        String deleteResourceName = "loans/"+id;
        Response deleteResponse = delete(BASE_URL+deleteResourceName);
        return deleteResponse;
    }
    
    public Response getLoansOfAUserByUserId(int userId){
        LOG.info("Making a GET request to retrieve the loans of a user by his ID");
        String resourceName = "loans/ofuser/"+userId;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response getLoansOfBookByBookId(int bookId){
        LOG.info("Making a GET request to get loands related to a book by its ID");
        String resourceName = "loans/ofbook/"+bookId;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response getLoansOfUserAndBookByUsersAndBooksId(int userId, int bookId){
        LOG.info("Making a GET request to get the loans related to a book and a user by their IDs");
        String resourceName = "loans/ofuser/"+userId+"/ofbook/"+bookId;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
  
}
