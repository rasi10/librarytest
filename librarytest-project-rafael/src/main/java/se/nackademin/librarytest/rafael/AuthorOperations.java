
package se.nackademin.librarytest.rafael;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;    


public class AuthorOperations {
    private static final String BASE_URL = "http://localhost:8080/librarytest-rest/";
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    
    
    
    private String jsonString = "";
    
    public void setJsonString(String string){
        this.jsonString = string;
    }
    public String getJsonString(){
        return this.jsonString;
    }
    
    
     //supporting method for  getting all the books 
    public Response getAllAuthors(){
        LOG.info("Making a GET request to get all authors");
        String resourceName = "authors";
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response getAuthorById(int id){
        LOG.info("Making a GET request to get authors by his ID");
        String resourceName = "authors/"+id;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
     
    public int getIdOfTheLastAddedAuthor(){
        LOG.info("Making a GEt request to get the ID of the last created author");
        Response getResponse = getAllAuthors();           
        int fetchedId = getResponse.jsonPath().getInt("authors.author[-1].id");         
        return fetchedId;
    }
    
    public String getNameOfTheLastAddedAuthor(){
        LOG.info("Making a GET request to get the name of the last created author");
        Response getResponse = getAllAuthors();           
        String fetchedName = getResponse.jsonPath().getString("authors.author[-1].name");          
        return fetchedName;
    }
    
    public String getFirstNameOfTheLastAddedAuthor(){
        LOG.info("Making a GET request to get the first name of the last created author");
        Response getResponse = getAllAuthors();           
        String fetchedName = getResponse.jsonPath().getString("authors.author[-1].firstName");          
        return fetchedName;
    }
    public Response createRandomAuthorWithNoId(){
        LOG.info("Making a POST request to create an author with no ID");
        String resourceName = "authors";          
        String bio = UUID.randomUUID().toString();
        String country = UUID.randomUUID().toString();
        String firstName = UUID.randomUUID().toString();
        String lastName = UUID.randomUUID().toString();
        String postBodyTemplate = "{\n" +
            "    \"author\": {\n" +            
            "        \"bio\":\"%s\",\n" +
            "        \"country\":\"%s\",\n" +
            "        \"firstName\":\"%s\",\n" +
            "        \"lastName\":\"%s\",\n" +
            "    }\n" +
"}";
        
        String postBody = String.format(postBodyTemplate,bio,country,firstName,lastName);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;        
    } 
    public Response createRandomAuthorWithId(){
        LOG.info("Making a POST request to create an author with no ID");
        String resourceName = "authors";          
        String bio = UUID.randomUUID().toString();
        String country = UUID.randomUUID().toString();
        String firstName = UUID.randomUUID().toString();
        String lastName = UUID.randomUUID().toString();
        String postBodyTemplate = "{\n" +
            "    \"author\": {\n" +
            "        \"id\":\"%s\",\n" +
            "        \"bio\":\"%s\",\n" +
            "        \"country\":\"%s\",\n" +
            "        \"firstName\":\"%s\",\n" +
            "        \"lastName\":\"%s\",\n" +
            "    }\n" +
            "}";
                
        
        String postBody = String.format(postBodyTemplate,new Random().nextInt(10000),bio,country,firstName,lastName);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;        
    }
    
    public Response createInvalidAuthorExistingIdOnTheDatabase(int id){
        LOG.info("Making a POST request with an author that alread exists");
        String resourceName = "authors";          
        String name = UUID.randomUUID().toString();
        String postBodyTemplate = "{\n" +
                "    \"author\": {\n" +
                "        \"id\":\"%s\",\n" +
                "        \"bio\": \"bio.\",\n" +
                "        \"country\": \"country\",\n" +
                "        \"firstName\": \"name\",\n" +
                "        \"lastName\": \"last name\"\n" +
                "    }\n" +
                "}";

        
        String postBody = String.format(postBodyTemplate, id);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;        
    }
    
     //supporting method for deleting an author
    public Response deleteAuthor(int id){
        LOG.info("Making a DELETE request to delete an author by his ID");
        String deleteResourceName = "authors/"+id;
        Response deleteResponse = delete(BASE_URL+deleteResourceName);
        return deleteResponse;
    }
    
    public Response editAuthorWithMyName(int id){
        LOG.info("Making a PUT request to edit the name with the author with my own name");
        String resourceName = "authors";                  
        String postBodyTemplate = "{\n" +
                "    \"author\": {\n" +
                "        \"id\":\"%s\",\n" +
                "        \"bio\": \"bio.\",\n" +
                "        \"country\": \"country\",\n" +
                "        \"firstName\":\"%s\",\n" +
                "        \"lastName\":\"%s\",\n" +
                "    }\n" +
                "}";
        
        String postBody = String.format(postBodyTemplate, id, "Rafael", "Silva");
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).put(BASE_URL +resourceName);
        return postResponse; 

    }
}
