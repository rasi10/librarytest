package se.nackademin.librarytest.rafael;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import java.util.UUID;
import java.util.logging.Logger;

/**
 *
 * @author rafael
 */
public class BookOperations {
    private static final String BASE_URL = "http://localhost:8080/librarytest-rest/";
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    
    private String jsonString = "";
    
    public void setJsonString(String string){
        this.jsonString = string;
    }
    public String getJsonString(){
        return this.jsonString;
    }
    
    public BookOperations(){
        
    }
    
    //supporting method for  getting all the books 
    public Response getAllBooks(){
        LOG.info("Making a GET request to retrieve all books");
        String resourceName = "books";
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response createRandomBookWithAuthorId(int authorId){
        LOG.info("Making a POST request to create an book with ID of the author informed");
        String resourceName = "books";
        
        String postBodyTemplate = "{\n" +
                    "  \"book\": {\n" +
                    "    \"author\": {\n" +
                    "      \"id\":\"%s\",\n" +
                    "    },\n" +
                    "    \"description\": \"Book description...\",\n" +
                    "    \"isbn\": \"0-575-01587-X\",\n" +
                    "    \"nbrPages\": 256,\n" +
                    "    \"publicationDate\": \"1973-06-28\",\n" +
                    "    \"title\": \"Rendezvous with Rama\",\n" +
                    "    \"totalNbrCopies\": 4\n" +
                    "  }\n" +
                    "}";
        
        String postBody = String.format(postBodyTemplate, authorId);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;
    } 
        
    
    public Response updateBookWithAuthorId(int bookId){
        LOG.info("Making a PUT request to update a book by its ID");
        String resourceName = "books";
        String updatedDescription = UUID.randomUUID().toString().substring(0,3);        
        String postBodyTemplate = "{\n" +
                    "    \"book\": {\n" +
                    "        \"id\":\"%s\",\n" +
                    "        \"description\":\"%s\",\n" +
                    "        \"isbn\": \"0-06-051518-X\",\n" +
                    "        \"nbrPages\": 400,\n" +
                    "        \"publicationDate\": \"2005-11-20\",\n" +
                    "        \"title\": \"Anansi Boys\",\n" +
                    "        \"totalNbrCopies\": 1\n" +
                    "    }\n" +
                    "}";
        
        String putBody = String.format(postBodyTemplate, bookId, "Updated Book"+updatedDescription);
        setJsonString(putBody);
        
        Response putResponse = given().contentType(ContentType.JSON).body(putBody).put(BASE_URL +resourceName);
        return putResponse;
    } 
    
    
    public Response updateBookWithAuthorIdInvalidBook(int bookId){
        LOG.info("Making a PUT request to update a book by its ID - book with no title");
        String resourceName = "books";
        String updatedDescription = UUID.randomUUID().toString().substring(0,3);        
        String postBodyTemplate = "{\n" +
                    "    \"book\": {\n" +
                    "        \"id\":\"%s\",\n" +
                    "        \"description\":\"%s\",\n" +
                    "        \"isbn\": \"0-06-051518-X\",\n" +
                    "        \"nbrPages\": 400,\n" +
                    "        \"publicationDate\": \"2005-11-20\",\n" +                    
                    "        \"totalNbrCopies\": 1\n" +
                    "    }\n" +
                    "}";
        
        String putBody = String.format(postBodyTemplate, bookId, "Updated Book"+updatedDescription);
        setJsonString(putBody);
        
        Response putResponse = given().contentType(ContentType.JSON).body(putBody).put(BASE_URL +resourceName);
        return putResponse;
    } 
    public int getIdOfTheLastAddedBook(){
        LOG.info("Making a GET request to get the id of the last added book");
        Response getResponse = getAllBooks();           
        int fetchedId = getResponse.jsonPath().getInt("books.book[-1].id");         
        return fetchedId;
    }
    
    //supporting method for returning the status code of a response
    public int getStatusCodeFromResponse(Response response){
        LOG.info("Getting the status code of the response");
        return response.getStatusCode();
    }
    
     //supporting method for deleting a book
    public Response deleteBookById(int id){
        LOG.info("Making a DELETE request to delete a book by its ID");
        String deleteResourceName = "books/"+id;
        Response deleteResponse = delete(BASE_URL+deleteResourceName);
        return deleteResponse;
    }
    
      //supporting method for  getting all the books. 
    public Response getABookById(int id){
        LOG.info("Making a GET request to get a book by its ID");
        String resourceName = "books/"+id;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
     public Response getBooksByAuthorId(int id){
         LOG.info("Making a GEt request to get a list of books by its author's id");
        String resourceName = "books/byauthor/"+id;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response getAuthorsByBookId(int id){
        LOG.info("Making a GET request to get the authors of a book by its ID");
        String resourceName = "books/"+id+"/authors";
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response addAnAuthorToABook(int authorId, int bookId){
        LOG.info("Making a POST request to add an author to a book");
        String resourceName = "books/"+bookId+"/authors";
        
        String postBodyTemplate = "{\n" +
                    "    \"author\": {\n" +
                    "        \"id\":\"%s\",\n" +
                    "    }\n" +
                    "}";
        
        String postBody = String.format(postBodyTemplate, authorId);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;
    }
    
    public Response editListOfAuthorsToABook(int authorId1, int authorId2, int bookId){
        LOG.info("Making a PUT request to edit the list of authors of a book");
        String resourceName = "books/"+bookId+"/authors";        
        String postBodyTemplate = "{\n" +
                    "    \"authors\": {\n" +
                    "        \"author\": [\n" +
                    "            {\n" +
                    "                \"id\":\"%s\",\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"id\":\"%s\",\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}";
        
        String postBody = String.format(postBodyTemplate, authorId1, authorId2);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).put(BASE_URL +resourceName);
        return postResponse;
    }
   
    
}
