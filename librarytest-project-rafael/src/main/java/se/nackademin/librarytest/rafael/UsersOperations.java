/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin.librarytest.rafael;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import java.util.logging.Logger;
import java.util.UUID;

/**
 *
 * @author rafael
 */
public class UsersOperations {
    
    private static final String BASE_URL = "http://localhost:8080/librarytest-rest/";
    private static final Logger LOG = Logger.getLogger(UsersOperations.class.getName());
    
    private String jsonString = "";
    
    public void setJsonString(String string){
        this.jsonString = string;
    }
    public String getJsonString(){
        return this.jsonString;
    }
    
    
    
    public Response getAllUsers(){
        LOG.info("Making a GET request to retrieve all users");
        String resourceName = "users";
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response createANewUser(){
        LOG.info("Making a POST request to create a new user");
        String resourceName = "users";
        String userName = UUID.randomUUID().toString();
        
        String postBodyTemplate = "{\n" +
                "  \"user\": {\n" +
                "    \"displayName\":\"%s\",\n" +
                "    \"password\":\"%s\",\n" +
                "    \"role\":\"LOANER\",\n" +
                "  }\n" +
                "}";

        String postBody = String.format(postBodyTemplate, userName, userName);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;
    } 
    
    public Response createANewUserWithNoPassword(){
        LOG.info("Making a POST request to create a new user");
        String resourceName = "users";
        String userName = UUID.randomUUID().toString();
        
        String postBodyTemplate = "{\n" +
                "  \"user\": {\n" +
                "    \"displayName\":\"%s\",\n" +              
                "    \"role\":\"LOANER\",\n" +
                "  }\n" +
                "}";

        String postBody = String.format(postBodyTemplate, userName);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).post(BASE_URL +resourceName);
        return postResponse;
    } 
     
    public Response editUserNewUser(int userId, String displayName, String password){
        LOG.info("Making a PUT request to edit a user's email");
        String resourceName = "users";        
        
        String postBodyTemplate = "{\n" +
            "    \"user\": {\n" +
            "        \"id\":\"%s\",\n" +
            "        \"displayName\":\"%s\",\n" +
            "         \"email\": \"editedemail@email.com\",\n" +
            "        \"password\":\"%s\",\n" +
            "        \"role\": \"LOANER\"\n" +
            "    }\n" +
            "}";

        String postBody = String.format(postBodyTemplate, userId, "Rafael"+displayName.substring(0,4), password);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).put(BASE_URL +resourceName);
        return postResponse;
    }
    
    public Response editUserNewUserNoNameNoPassword(int userId){
        LOG.info("Making a PUT request to edit a user's email - without displayname and password fields");
        String resourceName = "users";        
        
        String postBodyTemplate = "{\n" +
            "    \"user\": {\n" +
            "        \"id\":\"%s\",\n" +
            "         \"email\": \"editedemail@email.com\",\n" +
            "        \"role\": \"LOANER\"\n" +
            "    }\n" +
            "}";

        String postBody = String.format(postBodyTemplate, userId);
        setJsonString(postBody);
        
        Response postResponse = given().contentType(ContentType.JSON).body(postBody).put(BASE_URL +resourceName);
        return postResponse;
    }
    
     
    public int getIdOfTheLastAddedUser(){
        LOG.info("Retrieving the id of the last added user");
        Response getResponse = getAllUsers();           
        int fetchedId = getResponse.jsonPath().getInt("users.user[-1].id");         
        return fetchedId;
    }
    
    public String getNameOfTheLastAddedUser(){
        LOG.info("Retrieving the name of the last added user");
        Response getResponse = getAllUsers();           
        String fetchedName = getResponse.jsonPath().getString("users.user[-1].displayName");         
        return fetchedName;
    }
    
    public String getEmailOfTheLastAddedUser(){
        LOG.info("Retrieving the email of the last added user");
        Response getResponse = getAllUsers();           
        String fetchedName = getResponse.jsonPath().getString("users.user[-1].password");         
        return fetchedName;
    }
        
    public Response getAUserById(int id){
        LOG.info("Making a GET request to get a user by his ID");
        String resourceName = "users/"+id;
        Response getResponse = given().accept(ContentType.JSON).get(BASE_URL + resourceName);//.prettyPeek();
        return getResponse;
    }
    
    public Response deleteBookById(int id){
        LOG.info("Making a DELETE request to delete a user by his ID");
        String deleteResourceName = "users/"+id;
        Response deleteResponse = delete(BASE_URL+deleteResourceName);
        return deleteResponse;
    }
}
